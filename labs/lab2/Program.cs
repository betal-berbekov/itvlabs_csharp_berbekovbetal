﻿using System;
using System.ComponentModel;

namespace lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            part1();
            part2();
            Console.ReadLine();
        }

        static private void part1()
        {
            GetValue("a", out int a);
            GetValue("b", out int b);
            GetValue("c", out int c);
            GetValue("n", out int n);
            GetValue("m", out int m);
            GetValue("d", out int d);

            int discr = (int)Math.Sqrt((double)(b * b - 4 * a * c)); // discriminant 

            int x1 = ((-b + discr) / 2 * a);
            int x2 = ((-b - discr) / 2 * a);

            double x = -(n / m);

            int input = x1 == d || x2 == d || (int)x == d ? 1 : 0;
            Console.WriteLine($"Result = {input}\n");
        }


        static private void part2()
        {
            GetValue("distance", out int distance);
            GetValue("gas consumption", out double consumption);
            GetValue("gas cost", out double cost);
            
            distance *= 2;
            double volume = consumption * distance / 100;

            double wasted = volume * cost;
            humanize(wasted);
        }

        static private void GetValue<T>(string name, out T result)
        {
            bool flag = false;

            result = default(T);
            while (!flag)
            {
                try
                {
                    var converter = TypeDescriptor.GetConverter(typeof(T));
                    Console.Write($"{name} = ");
                    result = (T)converter.ConvertFromString(Console.ReadLine());
                    flag = true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid value. Please, try again input 'int' or 'double' value");
                }
            }
        }

        static private void humanize(double number)
        {
            double tmp = Math.Round(number, 2);
            int integer = (int)Math.Truncate(tmp);
            int rest = (int)(tmp * 100 % 100);
            Console.WriteLine($"{integer}rub {rest}kop");
        }
    }
}
