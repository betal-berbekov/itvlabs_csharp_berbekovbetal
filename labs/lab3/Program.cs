﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static lab3.BubbleSorter;
using static lab3.MergeSort;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            List<double> unsortedList = new List<double>();
            
            for (int i=0; i<100; i++)
            {
                unsortedList.Add(rnd.Next(0, 100));
            }

            BubbleSorter b = new BubbleSorter();
            List<double> currentList = b.Sort(new List<double>(unsortedList));

            //var merged_sorted_list = MergeSort.Sort(unsorted_list);
            Console.WriteLine($"{string.Join(", ", currentList)}");

            //Console.WriteLine($"{currentList} bubble compare = {BubbleSorter.compare_counter}");
            //Console.WriteLine($"merge compare = {MergeSort.compare_counter}");
            Console.ReadLine();
        }
    }
}
