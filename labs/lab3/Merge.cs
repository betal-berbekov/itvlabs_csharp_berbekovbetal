﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    public class MergeSort
    {
        public static int compare_counter = 0;
        
        public List<double> Sort(List<double> unsorted_list)
        {
            if (unsorted_list.Count == 1)
            {
                return unsorted_list;
            }

            int mid_point = unsorted_list.Count / 2;
            var left = unsorted_list.Take(mid_point).ToList();
            var right = unsorted_list.Skip(mid_point).ToList();

            return Merge(Sort(left), Sort(right), unsorted_list);
        }

        static List<double> Merge(List<double> left, List<double> right, List<double> list)
        {
            int index = 0;
            while (left.Count > 0 && right.Count > 0)
            {
                compare_counter++;
                if (left[0] < right[0])
                {
                    list[index++] = left.First();
                    left.RemoveAt(0);
                }
                else
                {
                    list[index++] = right.First();
                    right.RemoveAt(0);
                }
            }

            while (left.Count > 0)
            {
                list[index++] = left.First();
                left.RemoveAt(0);
            }

            while (right.Count > 0)
            {
                list[index++] = right.First();
                right.RemoveAt(0);
            }

            return list;
        }

    }
}
